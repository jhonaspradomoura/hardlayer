#include "bit.hpp"

void Bit::set(volatile uint8_t *reg, uint8_t pos)
{
    *reg |= (1<<pos);
}

void Bit::erase(volatile uint8_t *reg, uint8_t pos)
{
    *reg &= ~(1<<pos) ;
}


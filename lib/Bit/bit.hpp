#ifndef HARDLAYER_BIT_HPP
#define HARDLAYER_BIT_HPP

#include <stdint.h>

namespace Bit
{
    /**
     * Sets pos in reg to 1.
     * @param reg (volatile uint8_t*) - register you wish to alter.
     * @param pos (uint8_t)           - position on byte you wish to set to 1.
     */
    void set(volatile uint8_t *reg, uint8_t pos);
    
    /**
     * Sets pos in reg to 0.
     * @param reg (volatile uint8_t*) - register you wish to alter.
     * @param pos (uint8_t)           - bit you wish to set to 0.
     */
    void erase(volatile uint8_t *reg, uint8_t pos);
}

#endif //HARDLAYER_BIT_HPP

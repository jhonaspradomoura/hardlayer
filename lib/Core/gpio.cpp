#include "gpio.hpp"

#if defined(ARDUINO_AVR_MEGA2560) || defined(ARDUINO_AVR_MEGA)
/**
 * Array of all the available pin groups linking to its register.
 */
const volatile uint8_t *GPIO::groupAll[11] = {&PINA, &PINB, &PINC, &PIND, &PINE, &PINF, &PING, &PINH, &PINJ, &PINK,
                                                  &PINL};


#else
volatile uint8_t *GPIO::groupAll[3] = {&PINB, &PINC, &PIND};
#endif


void GPIO::setPinMode(IO_GROUP group, uint8_t pin, MODE mode)
{
    auto address = GPIO::groupAll[group];
    switch (mode)
    {
        case OUTPUT_MODE:
            Bit::set(regDDR(address), pin);
            break;
        case INPUT_MODE:
            Bit::erase(regDDR(address), pin);
            break;
    }
}


void GPIO::setPullUp(IO_GROUP group, uint8_t pin, STATE state)
{
    auto address = groupAll[group];
    switch (state)
    {
        case OFF:
            Bit::erase(regPORT(address), pin);
            break;
        case ON:
            Bit::set(regPORT(address), pin);
            break;
    }
}

void GPIO::setPinLevel(IO_GROUP group, uint8_t pin, LEVEL lvl)
{
    auto address = groupAll[group];
    switch (lvl)
    {
        case LOW:
            Bit::erase(regPORT(address), pin);
            break;
        case HIGH:
            Bit::set(regPORT(address), pin);
            break;
    }
}



#ifndef HARDLAYER_GPIO_HPP
#define HARDLAYER_GPIO_HPP

#include <stdint.h>
#include <avr/io.h>
#include "bit.hpp"

enum MODE { OUTPUT_MODE, INPUT_MODE };
enum LEVEL { LOW, HIGH };
enum STATE { OFF, ON };

#if defined(ARDUINO_AVR_MEGA2560) || defined(ARDUINO_AVR_MEGA)
enum IO_GROUP { A, B, C, D, E, F, G, H, J, K, L };
#else
enum IO_GROUP { B, C, D};
#endif

#define   regPIN(ioGroup)                               (ioGroup)
#define   regDDR(ioGroup)                               ((volatile uint8_t*) ioGroup + 1)
#define   regPORT(ioGroup)                              ((volatile uint8_t*) ioGroup + 2)

class GPIO
{
public:
    static void setPinMode(IO_GROUP group, uint8_t pin, MODE mode);
    
    static void setPullUp(IO_GROUP group, uint8_t pin, STATE state);
    
    static void setPinLevel(IO_GROUP group, uint8_t pin, LEVEL lvl);


private:
    GPIO() = default;
    
    #if defined(ARDUINO_AVR_MEGA2560) || defined(ARDUINO_AVR_MEGA)
    static const volatile uint8_t *groupAll[11];
    #else
    static volatile uint8_t groupAll[3];
    #endif
};


#endif //HARDLAYER_GPIO_HPP
